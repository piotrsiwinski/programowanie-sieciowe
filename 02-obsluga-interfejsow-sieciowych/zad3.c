#include <arpa/inet.h>
#include <linux/if.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <unistd.h>

int main(int argc, char ** argv) {
    int sfd;
    struct ifreq ifr;

    sfd = socket(PF_INET, SOCK_DGRAM, 0);
    strncpy(ifr.ifr_name, argv[1], IFNAMSIZ);

    ioctl(sfd, SIOCGIFFLAGS, &ifr);

    char * command = argv[2];
    if(strcmp(command, "up") == 0){
        ifr.ifr_flags |= IFF_UP;
        ioctl(sfd, SIOCSIFFLAGS, &ifr);
    } else if(strcmp(command, "down") == 0){
        ifr.ifr_flags &= ~IFF_UP;
        ioctl(sfd, SIOCSIFFLAGS, &ifr);
    }else {
        printf("Wrong command");
    }
}