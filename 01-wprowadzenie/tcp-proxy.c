/*                                                                                                                  * Zajecia 1 - Zadanie 6 - Proxy TCP                           
 * Compilation:  gcc -Wall ./tcp-client.c -o ./tcp-client                  
 * Usage:        ./tcp-client MY_PORT SERVER_TO_CONNECT SERVER_PORT                                  
 *                                                                                                                                               
 */                                                                        

#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char **argv) {

    int sfd, cfd, nextfd, rc, on = 1;
    socklen_t sl;
    struct sockaddr_in saddr, caddr, nextaddr;
    struct hostent *addrent;
    char buf[255] = {0};

    memset(&saddr, 0, sizeof(saddr));
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(1234);
    saddr.sin_addr.s_addr = INADDR_ANY;
    sfd = socket(AF_INET, SOCK_STREAM, 0);
    setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, (char *) on, sizeof(on));
    bind(sfd, (struct sockaddr *) &saddr, sizeof(saddr));
    listen(sfd, 5);
    while (1) {
        memset(&cfd, 0, sizeof(cfd));
        sl = sizeof(caddr);
        cfd = accept(sfd, (struct sockaddr *) &caddr, &sl);
        rc = read(cfd, buf, 255);
        printf("Message: %s", buf);
        close(cfd);
        memset(&nextaddr, 0, sizeof(nextaddr));
        // connect to next client
        if (argc < 3) {
            printf("Required parameters are missing. Please type address and port as command line arguments");
            return -1;
        }
        addrent = gethostbyname(argv[1]);
        nextaddr.sin_family = AF_INET;
        nextaddr.sin_port = htons(atoi(argv[2]));
        memcpy(&nextaddr.sin_addr.s_addr, addrent->h_addr, addrent->h_length);
        nextfd = socket(AF_INET, SOCK_STREAM, 0);
        connect(nextfd, (struct sockaddr *) &nextaddr, sizeof(nextaddr));
        write(nextfd, buf, rc);
        close(nextfd);
    }
    close(sfd);
    return EXIT_SUCCESS;
}
