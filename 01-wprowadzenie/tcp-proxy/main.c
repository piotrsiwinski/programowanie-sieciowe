/*
 * Zajecia 1 - Zadanie 6 - Proxy TCP
 * Compilation:  gcc -Wall ./tcp-client.c -o ./tcp-client                  
 * Usage:        ./tcp-client MY_PORT SERVER_TO_CONNECT SERVER_PORT                                  
 *                                                                                                                                               
 */

#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>


int main(int argc, char **argv) {
    if (argc < 4) {
        printf("Required parameters are missing. Please type MY_PORT SERVER_TO_CONNECT SERVER_PORT");
        return -1;
    }

    int cfd, nextfd, on = 1;
    socklen_t sl;
    struct sockaddr_in saddr, caddr, nextaddr;
    struct hostent *addrent;
    char buf[255];
    int serverPort = atoi(argv[1]);

    memset(&saddr, 0, sizeof(saddr));
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(serverPort);
    saddr.sin_addr.s_addr = INADDR_ANY;
    int sfd = socket(AF_INET, SOCK_STREAM, 0);
    setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, (char *) on, sizeof(on));
    if (bind(sfd, (struct sockaddr *) &saddr, sizeof(saddr)) == -1) {
        printf("Cannot bind port");
        return EXIT_FAILURE;
    }
    if (listen(sfd, 5) == -1) {
        printf("Cannot listen");
        return EXIT_FAILURE;
    }
    printf("Listening on port %d\n", serverPort);
    while (1) {
        memset(&cfd, 0, sizeof(cfd));
        sl = sizeof(caddr);
        if ((cfd = accept(sfd, (struct sockaddr *) &caddr, &sl)) == -1) {
            printf("Cannot accept client");
            return EXIT_FAILURE;
        }
        int bytesRead = read(cfd, buf, 255);
        printf("Received message: %s", buf);
        close(cfd);
        memset(&nextaddr, 0, sizeof(nextaddr));

        addrent = gethostbyname(argv[2]);
        nextaddr.sin_family = AF_INET;
        nextaddr.sin_port = htons(atoi(argv[3]));
        memcpy(&nextaddr.sin_addr.s_addr, addrent->h_addr, addrent->h_length);
        nextfd = socket(AF_INET, SOCK_STREAM, 0);
        if (connect(nextfd, (struct sockaddr *) &nextaddr, sizeof(nextaddr)) == -1) {
            printf("Cannot connect to next client");
            return EXIT_FAILURE;
        };
        if (write(nextfd, buf, bytesRead) == -1) {
            printf("Cannot write to client");
            return EXIT_FAILURE;
        };
        close(nextfd);
    }
    close(sfd);
    return EXIT_SUCCESS;
}
