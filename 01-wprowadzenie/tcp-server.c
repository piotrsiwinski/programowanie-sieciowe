#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

int main() {

    int sfd, cfd, on = 1;
    socklen_t sl;
    struct sockaddr_in saddr, caddr;

    memset(&saddr, 0, sizeof(saddr));
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(1234);
    saddr.sin_addr.s_addr = INADDR_ANY;
    sfd = socket(AF_INET, SOCK_STREAM, 0);
    setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, (char *) on, sizeof(on));
    bind(sfd, (struct sockaddr *) &saddr, sizeof(saddr));
    listen(sfd, 5);
    while (1) {
        memset(&cfd, 0, sizeof(cfd));
        sl = sizeof(caddr);
        cfd = accept(sfd, (struct sockaddr *) &caddr, &sl);
        write(cfd, "Hello world", 14);
        close(cfd);
    }
    close(sfd);
    return EXIT_SUCCESS;
}
