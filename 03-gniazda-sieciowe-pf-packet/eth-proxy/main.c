/*
 * Copyright (C) 2018 Michal Kalewski <mkalewski at cs.put.poznan.pl>
 *
 * Compilation:  gcc -Wall ./eth-proxy.c -o ./eth-proxy
 * Usage:        ./eth-proxy RECV_INTERFACE NEXT_INTERFACE DST_HW_ADDR
 * NOTE:         This program requires root privileges.
 *
 * Bug reports:  https://gitlab.cs.put.poznan.pl/mkalewski/ps-2018/issues
 *
 */

#include <arpa/inet.h>
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

#define ETH_P_CUSTOM 0x8888

int main(int argc, char **argv) {
    int recv_sfd;
    ssize_t recv_len;
    char *recv_frame;
    char *recv_fdata;

    struct ifreq recv_ifr;
    struct sockaddr_ll recv_sall;

    recv_sfd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_CUSTOM));
    strncpy(recv_ifr.ifr_name, argv[1], IFNAMSIZ);
    ioctl(recv_sfd, SIOCGIFINDEX, &recv_ifr);
    memset(&recv_sall, 0, sizeof(struct sockaddr_ll));
    recv_sall.sll_family = AF_PACKET;
    recv_sall.sll_protocol = htons(ETH_P_CUSTOM);
    recv_sall.sll_ifindex = recv_ifr.ifr_ifindex;
    recv_sall.sll_hatype = ARPHRD_ETHER;
    recv_sall.sll_pkttype = PACKET_HOST;
    recv_sall.sll_halen = ETH_ALEN;
    bind(recv_sfd, (struct sockaddr *) &recv_sall, sizeof(struct sockaddr_ll));
    while (1) {
        printf("Waiting for data\n");
        recv_frame = malloc(ETH_FRAME_LEN);
        memset(recv_frame, 0, ETH_FRAME_LEN);
        recv_fdata = recv_frame + ETH_HLEN;
        recv_len = recvfrom(recv_sfd, recv_frame, ETH_FRAME_LEN, 0, NULL, NULL);
        if (recv_len == -1) {
            printf("Error during receiving data\n");
            exit(-1);
        }
        printf("Received data: %s\n", recv_fdata);
        printf("\n");
//        free(recv_frame);
        /////////////////////////////////////////////////////////////////////

        int next_sfd, next_ifindex;
        char *next_frame;
        char *next_fdata;
        struct ethhdr *fhead;
        struct ifreq next_ifr;
        struct sockaddr_ll next_sall;

        next_sfd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_CUSTOM));
        next_frame = malloc(ETH_FRAME_LEN);
        memset(next_frame, 0, ETH_FRAME_LEN);
        fhead = (struct ethhdr *) next_frame;
        next_fdata = next_frame + ETH_HLEN;
        char *next = argv[2];
        strncpy(next_ifr.ifr_name, next, IFNAMSIZ);
        ioctl(next_sfd, SIOCGIFINDEX, &next_ifr);
        next_ifindex = next_ifr.ifr_ifindex;
        ioctl(next_sfd, SIOCGIFHWADDR, &next_ifr);
        memset(&next_sall, 0, sizeof(struct sockaddr_ll));
        next_sall.sll_family = AF_PACKET;
        next_sall.sll_protocol = htons(ETH_P_CUSTOM);
        next_sall.sll_ifindex = next_ifindex;
        next_sall.sll_hatype = ARPHRD_ETHER;
        next_sall.sll_pkttype = PACKET_OUTGOING;
        next_sall.sll_halen = ETH_ALEN;

        sscanf(argv[3], "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
               &next_sall.sll_addr[0], &next_sall.sll_addr[1], &next_sall.sll_addr[2],
               &next_sall.sll_addr[3], &next_sall.sll_addr[4], &next_sall.sll_addr[5]);
        memcpy(fhead->h_dest, &next_sall.sll_addr, ETH_ALEN);
        memcpy(fhead->h_source, &next_ifr.ifr_hwaddr.sa_data, ETH_ALEN);
        fhead->h_proto = htons(ETH_P_CUSTOM);
        memcpy(next_fdata, recv_fdata, strlen(recv_fdata));
        printf("Sending data: %s\n", next_fdata);
        int status = sendto(next_sfd, next_frame, ETH_HLEN + strlen(recv_fdata), 0,
                            (struct sockaddr *) &next_sall, sizeof(struct sockaddr_ll));
        if (status == -1) {
            printf("Cannot send data\n");
            exit(-1);
        } else {
            printf("Sended %dB\n", status);
        }
        printf("\n\n");

    }
    close(recv_sfd);
    return EXIT_SUCCESS;
}
