/*
 * Copyright (C) 2018 Michal Kalewski <mkalewski at cs.put.poznan.pl>
 *
 * Compilation:  gcc -Wall ./ethrecv.c -o ./ethrecv
 * Usage:        ./ethrecv INTERFACE
 * NOTE:         This program requires root privileges.
 *
 * Bug reports:  https://gitlab.cs.put.poznan.pl/mkalewski/ps-2018/issues
 *
 */

#include <arpa/inet.h>
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
// zmienic na wszystkie ramki
#define ETH_P_CUSTOM 0x8888


int main(int argc, char** argv) {
  int sfd, i;
  ssize_t len;
  char* frame;
  char* received_fdata;
  struct ethhdr* fhead;
  struct ifreq ifr;
  struct sockaddr_ll sall;

  sfd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_CUSTOM));
  strncpy(ifr.ifr_name, argv[1], IFNAMSIZ);
  ioctl(sfd, SIOCGIFINDEX, &ifr);
  memset(&sall, 0, sizeof(struct sockaddr_ll));
  sall.sll_family = AF_PACKET;
  sall.sll_protocol = htons(ETH_P_CUSTOM);
  sall.sll_ifindex = ifr.ifr_ifindex;
  sall.sll_hatype = ARPHRD_ETHER;
  sall.sll_pkttype = PACKET_HOST;
  sall.sll_halen = ETH_ALEN;
  bind(sfd, (struct sockaddr*) &sall, sizeof(struct sockaddr_ll));
  while(1) {
    frame = malloc(ETH_FRAME_LEN);
    memset(frame, 0, ETH_FRAME_LEN);
    fhead = (struct ethhdr*) frame;
      received_fdata = frame + ETH_HLEN;
    // tutaj zadeklarowac sockaddr_ll i jako pierwszy null wsk na ta strukture i rozmiar
      struct sockaddr_ll another;
      int size = sizeof(another);
    len = recvfrom(sfd, frame, ETH_FRAME_LEN, 0, (struct sockaddr *) &another, &size);
    printf("[%dB] %02x:%02x:%02x:%02x:%02x:%02x -> ", (int)len,
           fhead->h_source[0], fhead->h_source[1], fhead->h_source[2],
           fhead->h_source[3], fhead->h_source[4], fhead->h_source[5]);
    printf("%02x:%02x:%02x:%02x:%02x:%02x | ",
           fhead->h_dest[0], fhead->h_dest[1], fhead->h_dest[2],
           fhead->h_dest[3], fhead->h_dest[4], fhead->h_dest[5]);
    printf("%s\n", received_fdata);
    for (i = 0; i < len ; i++) {
      printf("%02x ", (unsigned char) frame[i]);
      if ((i + 1) % 16 == 0)
        printf("\n");
    }

      char* fdata;
      int cfd, ifindex;
      char* next_frame;
      cfd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_CUSTOM));
      next_frame = malloc(ETH_FRAME_LEN);
      memset(next_frame, 0, ETH_FRAME_LEN);
      fhead = (struct ethhdr*) next_frame;
      fdata = next_frame + ETH_HLEN;
      strncpy(ifr.ifr_name, argv[1], IFNAMSIZ);
      ioctl(cfd, SIOCGIFINDEX, &ifr);
      ifindex = ifr.ifr_ifindex;
      ioctl(cfd, SIOCGIFHWADDR, &ifr);
      memset(&sall, 0, sizeof(struct sockaddr_ll));
      sall.sll_family = AF_PACKET;
      sall.sll_protocol = htons(ETH_P_CUSTOM);
      sall.sll_ifindex = ifindex;
      sall.sll_hatype = ARPHRD_ETHER;
      sall.sll_pkttype = PACKET_OUTGOING;
      sall.sll_halen = ETH_ALEN;
//      sscanf(argv[2], "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
//             &sall.sll_addr[0], &sall.sll_addr[1], &sall.sll_addr[2],
//             &sall.sll_addr[3], &sall.sll_addr[4], &sall.sll_addr[5]);
      memcpy(fhead->h_dest, &sall.sll_addr, ETH_ALEN);
      memcpy(fhead->h_source, &ifr.ifr_hwaddr.sa_data, ETH_ALEN);
      fhead->h_proto = htons(ETH_P_CUSTOM);
      memcpy(fdata, received_fdata, strlen(received_fdata) + 1);
      sendto(cfd, next_frame, ETH_HLEN + strlen(received_fdata) + 1, 0,
             (struct sockaddr*) &sall, sizeof(struct sockaddr_ll));
      free(next_frame);
      close(cfd);




    printf("\n\n");
    free(frame);
  }
  close(sfd);
  return EXIT_SUCCESS;
}
