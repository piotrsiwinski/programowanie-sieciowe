
/* Obsługa tablicy routingu i pamięci podręcznej protokołu ARP
 * Compilation:  gcc -Wall ./main.c -o ./ireceiver -lpcap
 * Program ir send albo ustawia IP na danej karcie sieciowej (aktualne wywolanie)
 * albo konfiguruje tablice routingu
 */

#include <pcap.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
//#include <libnet.h>
#include <net/ethernet.h>
#include <sys/ioctl.h>
#include <linux/route.h>

#define ETH_P_CUSTOM 0x8888
#define IRI_T_ADDRESS 0
#define IRI_T_ROUTE   1

struct ifrtinfo {
    int iri_type;
    char iri_iname[16];
    struct sockaddr_in iri_iaddr; /* IP address */
    struct sockaddr_in iri_rtdst; /* dst. IP address */
    struct sockaddr_in iri_rtmsk; /* dst. netmask */
    struct sockaddr_in iri_rtgip; /* gateway IP */
};

char *errbuf;
pcap_t *handle;

void cleanup();

void stop(int signo);

void callback(u_char *user, const struct pcap_pkthdr *h, const u_char *packet);

void if_setup(struct ifrtinfo *iri);

void arp_setup(struct ifrtinfo *iri);

void handle_custom_packet(const u_char *packet);

int main(int argc, char **argv) {
    atexit(cleanup);
    signal(SIGINT, stop);
    errbuf = malloc(PCAP_ERRBUF_SIZE);
    handle = pcap_create(argv[1], errbuf);
    pcap_set_promisc(handle, 1);
    pcap_set_snaplen(handle, 65535);
    pcap_activate(handle);
    pcap_loop(handle, -1, callback, NULL);
}


void callback(u_char *user, const struct pcap_pkthdr *h, const u_char *packet) {
    printf("[%dB of %dB]\n", h->caplen, h->len);

    struct ether_header *eth_header;
    eth_header = (struct ether_header *) packet;

    if (ntohs(eth_header->ether_type) == ETH_P_CUSTOM) {
        handle_custom_packet(packet);
    }
}

void handle_custom_packet(const u_char *packet) {
    printf("ETH CUSTOM RECEIVED\n");
    struct ifrtinfo *info;
    info = (struct ifrtinfo *) (packet + sizeof(struct ether_header));
    printf("iri_iname: %s\n", info->iri_iname);

    if (info->iri_type == IRI_T_ADDRESS) {
        printf("IRI_T_ADDRESS received\n");
        if_setup(info);
    } else if (info->iri_type == IRI_T_ROUTE) {
        printf("IRI_T_ROUTE received\n");
        arp_setup(info);
    }
    printf("\n");
}

void if_setup(struct ifrtinfo *iri) {

    printf("Setting ip on interface %s\n", iri->iri_iname);

    int fd;
    struct ifreq ifr;
    struct sockaddr_in *sin;

    fd = socket(PF_INET, SOCK_DGRAM, 0);
    strncpy(ifr.ifr_name, iri->iri_iname, strlen(iri->iri_iname));
    sin = (struct sockaddr_in *) &ifr.ifr_addr;
    memset(sin, 0, sizeof(struct sockaddr_in));
    sin->sin_family = AF_INET;
    sin->sin_port = 0;
//    sin->sin_addr.s_addr = iri->iri_iaddr.sin_addr.s_addr;
    sin->sin_addr = iri->iri_iaddr.sin_addr;
    ioctl(fd, SIOCSIFADDR, &ifr);
    ioctl(fd, SIOCGIFFLAGS, &ifr);
    ifr.ifr_flags |= IFF_UP | IFF_RUNNING;
    ioctl(fd, SIOCSIFFLAGS, &ifr);
    close(fd);
}

void arp_setup(struct ifrtinfo *iri) {
    printf("Setting arp...");
    int fd;
    struct rtentry route;
    struct sockaddr_in *addr;

    fd = socket(PF_INET, SOCK_DGRAM, 0);
    memset(&route, 0, sizeof(route));

    addr = (struct sockaddr_in *) &route.rt_gateway;
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = iri->iri_rtgip.sin_addr.s_addr;

    addr = (struct sockaddr_in *) &route.rt_dst;
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = iri->iri_rtdst.sin_addr.s_addr;

    addr = (struct sockaddr_in *) &route.rt_genmask;
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = iri->iri_rtmsk.sin_addr.s_addr;

    route.rt_flags = RTF_UP | RTF_GATEWAY;
    route.rt_metric = 0;
    ioctl(fd, SIOCADDRT, &route);
    close(fd);
}

void cleanup() {
    pcap_close(handle);
    free(errbuf);
}

void stop(int signo) {
    exit(EXIT_SUCCESS);
}
