/*
 * Copyright (C) 2018 Michal Kalewski <mkalewski at cs.put.poznan.pl>
 *
 * Compilation:  gcc -Wall ./arpreq.c -o ./arpreq -lnet -pcap
 * Usage:        ./arpreq HOST
 * NOTE:         This program requires root privileges.
 *
 * Bug reports:  https://gitlab.cs.put.poznan.pl/mkalewski/ps-2018/issues
 *
 */

#include <libnet.h>
#include <stdlib.h>
#include <pcap/pcap.h>
#include <pcap.h>

struct arphdr {
	u_int16_t ftype;
	u_int16_t ptype;
	u_int8_t flen;
	u_int8_t plen;
	u_int16_t opcode;
	u_int8_t sender_mac_addr[6];
	u_int8_t sender_ip_addr[4];
	u_int8_t target_mac_addr[6];
	u_int8_t target_ip_addr[4];
};

int main(int argc, char** argv) {
	pcap_t* handle;
	struct pcap_pkthdr *header;
	const u_char *pkt_data;
  struct pcap_pkthdr **pkt_header;
  const u_char **pkt_data
  libnet_t *ln;
  u_int32_t target_ip_addr, src_ip_addr;
  u_int8_t bcast_hw_addr[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
           zero_hw_addr[6]  = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  struct libnet_ether_addr* src_hw_addr;
  char errbuf[LIBNET_ERRBUF_SIZE];

  ln = libnet_init(LIBNET_LINK, "br0", errbuf);
  src_ip_addr = libnet_get_ipaddr4(ln);
  src_hw_addr = libnet_get_hwaddr(ln);
  target_ip_addr = libnet_name2addr4(ln, argv[1], LIBNET_RESOLVE);
  libnet_autobuild_arp(
    ARPOP_REQUEST,                   /* operation type       */
    src_hw_addr->ether_addr_octet,   /* sender hardware addr */
    (u_int8_t*) &src_ip_addr,        /* sender protocol addr */
    zero_hw_addr,                    /* target hardware addr */
    (u_int8_t*) &target_ip_addr,     /* target protocol addr */
    ln);                             /* libnet context       */
  libnet_autobuild_ethernet(
    bcast_hw_addr,                   /* ethernet destination */
    ETHERTYPE_ARP,                   /* ethertype            */
    ln);                             /* libnet context       */
  libnet_write(ln);
 
  // get reply
  
	pcap_next_ex(handle, &header, &pkt_data);  
  
  	printf("received data");
  
  libnet_destroy(ln);
  return EXIT_SUCCESS;
}
