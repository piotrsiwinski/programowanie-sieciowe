/**
 * compile with gcc -Wall pcapdeviceinfo.c -lpcap -o pcapdeviceinfo
 * Program get information about device 
 * */
#include <stdio.h>
#include <pcap.h>
#include <arpa/inet.h>
#include <string.h>

int main(int argc, char **argv)
{
    char *device;
    char ip[20] = {0};
    char subnet_mask[20] = {0};
    bpf_u_int32 ip_raw;
    bpf_u_int32 subnet_mask_raw;
    char error_buffer[PCAP_ERRBUF_SIZE];

    /*Find a device */
    device = pcap_lookupdev(error_buffer);
    if (device == NULL)
    {
        printf("%s\n", error_buffer);
        return 1;
    }

    /* Get device info*/
    if (pcap_lookupnet(device, &ip_raw, &subnet_mask_raw, error_buffer) == -1)
    {
        printf(error_buffer);
        return 1;
    }

    /*Get ip in human readable form */
    struct in_addr address;
    address.s_addr = ip_raw;
    strcpy(ip, inet_ntoa(address));
    if (ip == NULL)
    {
        printf("ntoa error");
        return 1;
    }

    /*Get mask*/
    address.s_addr = subnet_mask_raw;
    strcpy(subnet_mask, inet_ntoa(address));

    printf("Device: %s\nIP Address: %s\nSubnet mask: %s\n", device, ip, subnet_mask);

    return 0;
}