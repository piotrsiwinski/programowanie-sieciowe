/*
 * Compilation:  gcc -Wall ./live_capture.c -o ./capture -lpcap
 * Usage:        ./capture INTERFACE
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pcap.h>
#include <netinet/in.h>
#include <netinet/if_ether.h>

void trap(u_char *user, const struct pcap_pkthdr *h, const u_char *bytes);

int main(int argc, char** argv){
    pcap_t *handle;
    char* error_buffer = malloc(PCAP_ERRBUF_SIZE);
    handle = pcap_create(argv[1], error_buffer);
    pcap_set_promisc(handle, 1);
    pcap_set_snaplen(handle, 65535);
    pcap_activate(handle);
    pcap_loop(handle, -1, trap, NULL);

    return 0;
}


void trap(u_char *user, const struct pcap_pkthdr *h, const u_char *bytes) {
  printf("[%dB of %dB] ", h->caplen, h->len);
  struct ether_header *eth_header;
  eth_header = (struct ether_header *) bytes;

  if(ntohs(eth_header -> ether_type) == ETHERTYPE_ARP){
      printf("arp \n");
  } else if(ntohs(eth_header -> ether_type) == ETHERTYPE_IP){
      printf("IP \n");
  }

  printf("\n");

}