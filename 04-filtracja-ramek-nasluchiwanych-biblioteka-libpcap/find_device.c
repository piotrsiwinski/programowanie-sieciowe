/**
 * compile with gcc -Wall find_device.c -lpcap -o find_device
 * Program finds the default device on which to capture
 * */
#include <stdio.h>
#include <pcap.h>

int main(int argc, char ** argv){
    char *device; /*Name of device*/
    char error_buffer[PCAP_ERRBUF_SIZE];

    device = pcap_lookupdev(error_buffer);
    if(device == NULL){
        printf("Error finding device: %s\n", error_buffer);
        return 1;
    }
    printf("Network device found %s\n", device);
    return 0;
}